INSERT INTO artists (name) VALUES 
("Taylor Swift"),
("Lady Gaga"),
("Justin Bieber"),
("Ariana Grande"),
("Bruno Mars");

/* 
Taylor Swift - id 3
Lady Gaga - id 4
Justin Bieber - id 5
Ariana Grande - id 6
Bruno Mars - id 7
*/

-- Taylor Swift
-- album 3
INSERT INTO albums 
(album_title, date_released, artist_id) 
VALUES 
("Fearless", "2008-01-01", 3);

INSERT INTO songs 
(song_name, length, genre, album_id) 
VALUES 
("Fearless", 246, "Pop rock", 3),
("Love Story", 213, "Country pop", 3);

INSERT INTO album 
(album_title, date_released, artist_id) 
VALUES 
("Red", 2012-01-01, 3);

INSERT INTO songs 
(song_name, length, genre, album_id)
VALUES
("State of Grace", 250, "Rock, Alternative rock, arena rock",4),
("Red", 204, "Country",4);


-- Lady Gaga
-- album 6
INSERT INTO albums 
(album_title, date_released, artist_id) 
VALUES 
("A Star is Born", "2018-01-01", 4);
INSERT INTO songs 
(song_name, length, genre, album_id) 
VALUES 
("Black Eyes", 181, "Rock and roll", 5),
("Shallow", 201, "Country, rock, folk rock", 5);

INSERT INTO albums 
(album_title, date_released, artist_id, 4) VALUES ("Born This Way", 2011-01-01, 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Born This Way", 252, "Electropop", 6);

-- Justin Bieber
-- album 7
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Purpose", "2015-01-01", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Sorry", 200, "Dancehall-poptropical housemoombathon", 7);

/* ADVANCE SELECTS */

-- Excluding records
SELECT * FROM songs WHERE id != 1;

-- Greater than, less than, or equal
SELECT * FROM songs WHERE id > 1;
SELECT * FROM songs WHERE id < 9;
SELECT * FROM songs WHERE id = 1;

-- OR
SELECT * FROM songs WHERE id = 1 OR id = 5;

-- IN
SELECT * FROM songs WHERE id IN (1, 3, 5);
SELECT * FROM songs WHERE genre IN ("OPM", "Electropop");

-- Combining COnditions
SELECT * FROM songs WHERE genre = "OPM" and  length > 230;

-- Find partial matches
-- ENDS WITH A
SELECT * FROM songs WHERE song_name LIKE "%a"; -- select keywords from the end

SELECT * FROM songs WHERE song_name LIKE "b%";
-- select keywords from the start

SELECT * FROM songs WHERE song_name LIKE "%a%";
-- select keywords in between 

-- select with format
SELECT * FROM songs WHERE date_released LIKE "201_-01-01";
SELECT * FROM songs WHERE date_released LIKE "201_-__-__";

-- Sorting records (ORDER BY)
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

-- DISTINCT
SELECT DISTINCT genre FROM songs; 

-- Table Joins
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id;

/* SELECT * FROM ReferenceTable JOIN tableName ON ReferenceTable.PrimaryKey = TableName.foreignKey */

SELECT album_title, artists.name FROM albums
	JOIN artists ON albums.artist_id = artists.id;

-- Combine more thant two tables
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

/* SELECT * FROM table1 
	JOIN table2 ON table1.primarykey = table2.foreignkey
	JOIN table3 ON table2.primarykey = table3.foreignkey */

SELECT songs.song_name FROM artists
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

-- Select columns to be included per table
SELECT artists.name, albums.album_title FROM artists
	JOIN albums ON artists.id = albums.artist_id;

/* UNION */
SELECT artist.name FROM artists UNION SELECT albums.album_title FROM albums

/* INNER JOIN */
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id

/* LEFT JOIN */
SELECT * FROM artists
	LEFT JOIN albums ON artist.id = albums.artist_id;

/* RIGHT JOIN */
SELECT * FROM albums
	RIGHT JOIN artists ON albums.artist_id = artists.id;

