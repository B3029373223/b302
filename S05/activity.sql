/* 
    - [SQL Capstone:]
        Create an activity.txt inside s05 project and create the SQL code for the following: 
    -
*/

-- No.1 [Return the customerName of the customers who are from the Philippines]
    SELECT customerName from customers WHERE country = "Philippines";
--

-- No.2 [Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"]
    SELECT contactLastName, contactFirstName  from customers WHERE customerName = "La Rochelle Gifts";
--

-- No.3 [Return the product name and MSRP of the product named "The Titanic"]
    SELECT productName, MSRP from products WHERE productName = "The Titanic";
--

-- No.4 [Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"]
    SELECT firstName, lastName  from employees WHERE email = "jfirrelli@classicmodelcars.com";
--

-- No.5 [Return the names of customers who have no registered state]
    SELECT customerName from customers WHERE state IS NULL;
--

-- No.6 [Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve]
    SELECT firstName, lastName, email  from employees WHERE lastName = "Patterson" AND firstName = "Steve";
--  

-- No.7 [Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000]
    SELECT customerName, country, creditLimit from customers WHERE country != "USA" AND creditLimit > 3000;
--

-- No.8 [Return the customer numbers of orders whose comments contain the string 'DHL']
    SELECT customerNumber from orders WHERE comments LIKE "%DHL%";
--

-- No.9 [Return the product lines whose text description mentions the phrase 'state of the art]
    SELECT productLine from productlines WHERE textDescription LIKE "%state of the art%";
--

-- No.10 [Return the countries of customers without duplication]
    SELECT DISTINCT country FROM customers;
--

-- No.11 [Return the statuses of orders without duplication]
    SELECT DISTINCT status FROM orders;
--

-- No.12 [Return the customer names and countries of customers whose country is USA, France, or Canada]
    SELECT customerName, country FROM customers WHERE country = "USA" OR country = "France" OR country = "Canada";
--

-- No.13 [Return the first name, last name, and office's city of employees whose offices are in Tokyo]
    SELECT firstName, lastName, offices.city FROM employees

	JOIN offices ON employees.officeCode = offices.officeCode

    WHERE offices.city = "Tokyo";
--

-- No.14 [Return the customer names of customers who were served by the employee named "Leslie Thompson"]
    SELECT customerName FROM customers

    JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber

    WHERE employees.firstName = "Leslie" AND employees.lastName = "Thompson";
--

-- No.15 [Return the product names and customer name of products ordered by "Baane Mini Imports"]
    SELECT products.productName, customers.customerName FROM orders

    JOIN customers ON orders.customerNumber = customers.customerNumber
    JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
    JOIN products ON orderdetails.productCode = products.productCode

    WHERE customers.customerName = "Baane Mini Imports";
--

-- No.16 [Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are located in the same country]
    SELECT firstName, lastName, customers.customerName FROM employees
     
    JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber 
    JOIN offices ON employees.officeCode = offices.officeCode 
    
    WHERE customers.country = offices.country;
--

-- No.17 [Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000.]
    SELECT productName, quantityInStock FROM products WHERE productline = "planes" AND quantityInStock < 1000;

-- No.18 [Return the customer's name with a phone number containing "+81"]
    SELECT customerName FROM customers WHERE phone LIKE "+81%"
--
