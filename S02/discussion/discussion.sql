/* 
    - [Command to show created Databases]
 */
SHOW DATABASES; 
/* 
    - [Command to Create a Database]
 */
CREATE DATABASE music_db;

/* 
    - [Command to Delete a Database]
 */
DROP DATABASE music_db;
/* 
    - [USE - Command to Select Database]
 */

 /* 
    - [Create a Table to the Selected Database]
        - id - name of the object
        - INT - (integer) or type of the object
        - NOT NULL - Not empty or null
        - AUTO_INCREMENT - Making object unque
    -
 */
USE music_db;

 CREATE TABLE users_account (
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    full_name VARCHAR(50) NOT NULL,
    contact_number INT NOT NULL,
    email VARCHAR(50),
    address VARCHAR(50),
    PRIMARY KEY (id)
);
/* 
    - [Command to Delete a Table in the Database]
 */
DROP TABLE users;

CREATE TABLE artists (
    id INT NOT NLL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE albums (
    id INT NOT NULL AUTO_INCREMENT,
    album_title VARCHAR(50) NOT NULL,
    date_released DATE NOT NULL,
    artist_id INT NOT NULL,
    -- Costraint / Rules
    PRIMARY KEY (id),
    CONSTRAINT fk_albums_artist_id
        FOREIGN KEY (artist_id) REFERENCES artists(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

CREATE TABLE songs(
    id INT NOT NULL AUTO_INCREMENT,
    song_name VARCHAR(50) NOT NULL,
    length TIME NOT NULL,
    genre VARCHAR(50) NOT NULL,
    album_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_songs_album_id
        FOREIGN KEY(album_id) REFERENCES albums(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

CREATE TABLE playlists(
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    datetime_created DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_playlists_user_id
        FOREIGN KEY(user_id) REFERENCES users_account(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

CREATE TABLE playlists_songs(
    id INT NOT NULL AUTO_INCREMENT,
    playlist_id INT NOT NULL,
    song_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_playlists_songs_playlist_id
        FOREIGN KEY(playlist_id) REFERENCES playlists(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT fk_playlists_songs_song_id
        FOREIGN KEY(song_id) REFERENCES songs(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);
