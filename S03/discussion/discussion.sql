/* [CRUD]
    INSERT - Create
 */

INSERT INTO artists (name) VALUES ("Rivermaya");
INSERT INTO artists (name) VALUES ("Psy");

/* INSERT INTO tableName (columnA, columnB, columnC) VALUES (columnAValue, columnBValue, columnCValue)*/
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Psy 6", "2021-1-1", 2);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Trip", "1991-1-1", 1);

INSERT INTO songs (songs_name, length, genre, album_id) VALUES ("Gangnam Style", 253, "K-pop", 1);
INSERT INTO songs (songs_name, length, genre, album_id) VALUES ("Kundiman", 234, "OPM", 2);
INSERT INTO songs (songs_name, length, genre, album_id) VALUES ("Kisapmata", 230, "OPM", 2);

/* [CRUD]
    SELECT - Retrieve
    * - All
 */

 SELECT * FROM songs;

/* Display the song_name and genre of all the songs */
 SELECT song_name, genre FROM songs;

 -- Mini Activity
 /* Display all song_names of songs tables and its album_id */

SELECT song_name, album_id FROM songs;

/* Display the song_name of all the OPM songs */
/* WHERE */
SELECT song_name from songs WHERE genre = "OPM";
SELECT song_name, genre from songs WHERE genre = "OPM";

/* [CRUD]
    UPDATE - UPDATE
    length - hour:minute:second ?
 */

/* UPDATE tableName set column = value WHERE column = value */
UPDATE songs SET length = 240 WHERE song_name = "Kundiman";

/* DELETE */
-- 
DELETE FROM songs WHERE genre = "OPM" AND length > 200;

/* AND */ /* All must be True for it to be true */
/* T and T and T and F will result to False*/

/* OR */
-- One true will result to True
/* T or T or T or F will result to True */
